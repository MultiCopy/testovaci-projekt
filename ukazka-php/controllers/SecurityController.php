<?php

class SecurityController extends Controller
{

    public function login(String $username, String $password) {
        if (isset($_POST['login'])) {

            $select = [
                'username', 'password'
            ];
            $condition = [
                'username' => $username
            ];

            $user = $this->db->get('User')->select($select)->where($condition)->result(true);
            if (!empty($user)) {
                $user = $user[0];
                $pwdCheck = password_verify($password, $user->getPassword());
                if ($pwdCheck == false) {
                    $this->redirect('/', ['error' => 'wrongpwd']);
                } else if ($pwdCheck == true) {

                    $_SESSION['user_id'] = $user->getId();
                    $_SESSION['user_username'] = $user->getUsername();

                    $this->redirect('/');
                } else {
                    $this->redirect('/admin/auth', ['error' => 'wrongpwd']);
                }
            } else {
                $this->redirect('/admin/auth', ['error' => 'wrongpwd']);
            }
        } else {
            $this->redirect('/admin/auth');
        }
    }


    public function logout() {
        session_unset();
        session_destroy();

        $this->redirect('/');
    }

    public function signup($username, $email, $password) {

        if (isset($_POST['signup'])) {

            $condition = [
                'username' => $username
            ];

            //$user = $this->db->condSelect('User', ['*'], $condition);
            if (!empty($user)) {
                header('Location: /signup?error=usertaken&username='.$username.'&email='.$email);
                exit();
            } else {

                $data = [
                    'username'  => $username,
                    'email'     => $email,
                    'password'  => password_hash($password, PASSWORD_DEFAULT)
                ];
                //$this->db->insert('User', $data);

                $this->redirect('/');
            }

        }
    }
}