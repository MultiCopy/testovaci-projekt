<?php

abstract class Controller {

    protected $db;
    protected $flashMsg;

    public function __construct() {
        $this->db = App::get('database');
        $this->flashMsg = new FlashMessages();
    }

    protected function redirect($location, $parameters = null) {

        $uriParams = '';
        if ($parameters !== null && !empty($parameters)) {
            $array = [];
            foreach ($parameters as $key => $value) {
                $array[] = "{$key}={$value}";
            }
            $uriParams = '?'.implode('&', $array);
        }

        header('Location: '.$location.$uriParams);
        exit();
    }

    protected function render($response) {

        if (array_key_exists('view', $response)) {
            $views = $_SERVER['DOCUMENT_ROOT'].'/views/';
            $extension = '.view.php';
            if (file_exists($views.$response['view'].$extension)) {

                if (array_key_exists('params', $response) && is_array($response['params'])) {
                    extract($response['params']);
                }

                $messages = $this->flashMsg;

                return require ($views.$response['view'].$extension);
            }
        }
    }

}