<?php

class PagesController extends Controller {

    public function home() {

        $this->render(['view' => 'pages/home']);
    }

    public function portfolio() {

        $condition = [
            'displayed' => '1'
        ];

        $tattoos = $this->db->get('Tattoo')->where($condition);

        $this->render([
            'view' => 'pages/portfolio',
            'params' => [
                'tattoos' => $tattoos
            ]
        ]);
    }

    public function about() {

        $this->render(['view' => 'pages/about']);
    }

    public function contact() {

        $this->render(['view' => 'pages/contact']);
    }

    public function signup() {

        $this->render(['view' => 'security/signup']);
    }

    public function adminAuthentication() {

        $this->render(['view' => 'security/login']);
    }

    public function adminProfile() {

        if (!isset($_SESSION['user_id'])) {
            $this->redirect('/');
        }

        $this->render(['view' => 'security/profile']);
    }

    public function adminTattoo() {

        if (!isset($_SESSION['user_id'])) {
            $this->redirect('/');
        }

        $tattoos = $this->db->get('Tattoo');

        $this->render([
            'view' => 'security/tattoo-admin',
            'params' => [
                'tattoos' => $tattoos
            ]
        ]);
    }

}