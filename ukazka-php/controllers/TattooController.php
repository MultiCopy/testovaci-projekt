<?php

class TattooController extends Controller {

    public function addTattoo($title, $description) {

        if (isset($_POST['upload-tattoo'])) {
            $file = $_FILES['image'];

            $name = $file['name'];
            $tmpName = $file['tmp_name'];
            $size = $file['size'];
            $error = $file['error'];

            $exploded = explode('.', $name);
            $extension = strtolower(end($exploded));

            $allowed = array('jpg', 'jpeg', 'png', 'gif');

            if (in_array($extension, $allowed)) {
                if ($error === 0) {
                    if ($size > 1000000) {
                        $uniqueName = uniqid('', true).".".$extension;
                        $destination = 'tattoos/'.$uniqueName;
                        move_uploaded_file($tmpName, $destination);

                        $data = [
                            'title' => $title == null ? '' : htmlspecialchars($title),
                            'image' => $uniqueName,
                            'description' => $description == null ? '' : htmlspecialchars($description)
                        ];
                        $this->db->get('Tattoo')->insert($data);
                    } else {
                        $this->redirect('/admin/tattoo', ['error' => 'size']);
                    }
                } else {
                    $this->redirect('/admin/tattoo', ['error' => 'file-upload']);
                }
            } else {
                $this->redirect('/admin/tattoo', ['error' => 'file-extension']);
            }
        }

        $this->redirect('/admin/tattoo');
    }

}