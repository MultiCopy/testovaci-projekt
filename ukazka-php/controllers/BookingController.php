<?php

use PHPMailer\PHPMailer\PHPMailer;

define('GUSER', 'monkey.donkey.tattoo@gmail.com'); // GMail username
define('GPWD', 'M0nk3yTatt00'); // GMail password

class BookingController extends Controller {

    public function book($contact, $description) {
  
        if (isset($_POST['book_submit'])) {

            $message = "
                <html>
                    <head>
                        <title>HTML email</title>
                    </head>
                    <body>
                        <h2>Somebody wants Tattoo!!</h2>
                        <span class=\"email-contact\">Contact: </span>
                        <span class=\"email-contact-text\">{$contact}</span>
                        <p class=\"email-description\">{$description}</p>
                    </body>
                </html>
            ";

            try {
                $mailer = new PHPMailer();

                // SMTP config
                $mailer->IsSMTP(); // enable SMTP
                $mailer->SMTPDebug = 0;  // debugging: 1 = errors and messages, 2 = messages only
                $mailer->SMTPAuth = true;  // authentication enabled
                $mailer->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
                $mailer->Host = 'smtp.gmail.com';
                $mailer->Port = 465;
                $mailer->Username = GUSER;
                $mailer->Password = GPWD;

                $mailer->setFrom('webmaster@tattoo.com');
                //$mailer->addAddress('denis.dovicic@gmail.com');
                //$mailer->addCC('lujkasak@gmail.com');
                $mailer->addCC('kuba.vybiral13@seznam.cz');
                $mailer->isHTML(true);
                $mailer->Subject = "Somebody wants a Tattoo!";
                $mailer->Body = $message;

                if (!$mailer->send()) {
                    $this->flashMsg->error('Message was not sent. Please try again...');
                } else {
                    $this->flashMsg->info('Message was sent to Tattoo Artist');
                }
            } catch (Exception $e) {
                $this->flashMsg->error('An error occured. Please try again...');
            }

            $this->redirect('/contact');
        }
    }

}