<?php


class Request
{

    public static function uri() {

        $urlPath = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

        return trim($urlPath, '/');
    }

    public static function method() {

        return $_SERVER['REQUEST_METHOD'];
    }
}