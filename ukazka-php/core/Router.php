<?php


class Router {

    protected $routes = [
        'GET' => [],
        'POST' => []
    ];

    public static function load($file) {

        $router = new static;
        require 'routes.php';

        return $router;
    }

    public function get($uri, $controller) {

        $this->routes['GET'][$uri] = $controller;
    }

    public function post($uri, $controller) {

        $this->routes['POST'][$uri] = $controller;
    }

    public function define($routes) {

        $this->routes = $routes;
    }

    public function direct($uri, $requestType) {

        if (array_key_exists($uri, $this->routes[$requestType])) {

            return $this->callAction(
                ...explode('@', $this->routes[$requestType][$uri]), ...[$requestType]
            );
        }

        throw new Exception('No route defined for this URI: '.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].'/'.$uri);
    }

    private function callAction($controller, $action) {

        $controller = new $controller;

        if (!method_exists($controller, $action)) {

            throw new Exception(get_class($controller)." doesn not respond to the {$action} action.");
        }

        $parameters = [];
        try {
            $r = new ReflectionMethod($controller, $action);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        foreach ($r->getParameters() as $param) {
            if (array_key_exists($param->getName(), $_POST)) {
                $parameters[] = htmlspecialchars($_POST[$param->getName()]);
            } else {
                $parameters[] = null;
            }
        }

        $controller->$action(...$parameters);

        return true;
    }
}