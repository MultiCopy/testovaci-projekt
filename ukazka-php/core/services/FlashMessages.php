<?php


class FlashMessages {

    const TYPES = [
        'INFO'    => 'i',
        'SUCCESS' => 's',
        'WARNING' => 'w',
        'ERROR'   => 'e'
    ];

    // Each message gets wrapped in this
    protected $msgWrapper = "<div class='%s'>%s</div>\n";

    protected $msgCssClass = 'alert';
    protected $cssClassMap = [
        self::TYPES['INFO']    => 'alert-info',
        self::TYPES['SUCCESS'] => 'alert-success',
        self::TYPES['WARNING'] => 'alert-warning',
        self::TYPES['ERROR']   => 'alert-danger'
    ];

    public function __construct() {

        if (!array_key_exists('flash_messages', $_SESSION)) {
            $_SESSION['flash_messages'] = [];
        }
    }

    public function info($message) {

        return $this->add($message, self::TYPES['INFO']);
    }

    public function success($message) {

        return $this->add($message, self::TYPES['SUCCESS']);
    }

    public function warning($message) {

        return $this->add($message, self::TYPES['WARNING']);
    }

    public function error($message) {

        return $this->add($message, self::TYPES['ERROR']);
    }

    private function add($message, $type) {
        if (!array_key_exists($type, $_SESSION['flash_messages']))
            $_SESSION['flash_messages'][$type] = [];
        $_SESSION['flash_messages'][$type][] = $message;

        return $this;
    }

    public function display($print = true) {

        if (!isset($_SESSION['flash_messages']))
            return false;

        $flashMessages = '';

        foreach (self::TYPES as $type) {
            if (isset($_SESSION['flash_messages'][$type])) {
                foreach ($_SESSION['flash_messages'][$type] as $message) {
                    $cssClass = $this->msgCssClass . ' ' . $this->cssClassMap[$type];

                    $flashMessages .= sprintf($this->msgWrapper, $cssClass, $message);
                }
            }
        }

        if ($print) {
            echo $flashMessages;
        } else {
            return $flashMessages;
        }

        $this->clear();

        return true;
    }

    protected function clear($types=[]) {
        if ((is_array($types) && empty($types)) || is_null($types) || !$types) {
            unset($_SESSION['flash_messages']);
        } elseif (!is_array($types)) {
            $types = [$types];
        }

        foreach ($types as $type) {
            unset($_SESSION['flash_messages'][$type]);
        }
    }
}