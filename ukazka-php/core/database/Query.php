<?php

class Query {

    const Q_SELECT = "SELECT %s FROM %s";
    const Q_INSERT = "INSERT INTO %s (%s) VALUES (%s)";
    const Q_DELETE = "DELETE FROM %s";
    const Q_UPDATE = "UPDATE %s SET %s";

    const Q_WHERE  = " WHERE %s";
    const Q_OR     = " OR %s";
    const Q_AND    = " AND %s";
    const Q_IN     = " IN %s";

    protected $query = '';

    public function clean() {

        $this->query = '';
    }

}