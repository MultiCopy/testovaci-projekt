<?php

class QueryBuilder extends Query {

    private $pdo;

    const SELECT = 0;
    const INSERT = 1;
    const DELETE = 2;
    const UPDATE = 3;

    const VALUES    = 0;
    const ENTITY    = 1;
    const TYPE      = 2;
    const CONDITION = 3;

    const WHERE = 0;

    private $qBuilder = [
        self::TYPE => self::SELECT,
        self::ENTITY => null,
        self::VALUES => null,
        self::CONDITION => null,
    ];

    private $qOptions = [
        self::WHERE => false
    ];

    private $placeholders;

    public function __construct(PDO $pdo) {
        $this->pdo = $pdo;
        $this->placeholders = [];
    }

    public function get($entity) {
        $this->qBuilder[self::ENTITY] = $entity;

        return $this;
    }

    public function select($columns) {
        $this->qBuilder[self::TYPE] = self::SELECT;

        if (is_array($columns)) {
            $this->qBuilder[self::VALUES] = $columns;
        }

        return $this;
    }

    public function insert($data) {
        $this->qBuilder[self::TYPE] = self::INSERT;
        $this->qBuilder[self::VALUES] = $data;
        $this->placeholders = array_merge($this->placeholders, $data);

        return $this;
    }

    public function delete() {
        $this->qBuilder[self::TYPE] = self::DELETE;

        return $this;
    }

    public function update($values) {
        $this->qBuilder[self::TYPE] = self::UPDATE;
        $this->qBuilder[self::VALUES] = $values;

        $set = array();
        foreach($values as $key => $value) {
            $set[] = $key.'=:'.$key;
        }
        $this->qBuilder[self::VALUES] = implode(', ', $set);

        $this->placeholders = array_merge($this->placeholders, $values);

        return $this;
    }

    public function where($condition) {
        $this->qOptions[self::WHERE] = true;

        $cond = array();
        foreach($condition as $key => $value) {
            $cond[] = $key.'=:'.$key;
        }
        $this->qBuilder[self::CONDITION] = implode(' AND ', $cond);

        $this->placeholders = array_merge($this->placeholders, $condition);

        return $this;
    }

    public function result($toEntity = false) {

        $statement = $this->execute();

        if ($toEntity) {
            $result = $statement->fetchAll(PDO::FETCH_CLASS, $this->qBuilder[self::ENTITY]);
        } else {
            $result = $statement->fetchAll();
        }

        return $result;
    }

    public function execute() {
        $this->build();

        try {
            $statement = $this->pdo->prepare($this->query);
            $statement->execute($this->placeholders);

            return $statement;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    private function build() {
        switch ($this->qBuilder[self::TYPE]) {
            case self::SELECT:

                $this->buildSelect();

                break;
            case self::INSERT:

                $this->buildInsert();

                break;
            case self::DELETE:

                $this->buildDelete();

                break;
            case self::UPDATE:

                $this->buildUpdate();

                break;
            default:
                die("not implemented");
                break;
        }
    }

    private function buildSelect() {

        if (is_null($this->qBuilder[self::VALUES])) {
            $this->qBuilder[self::VALUES] = ['*'];
        }
        $this->query = sprintf(
            self::Q_SELECT,
            implode(', ', $this->qBuilder[self::VALUES]),
            $this->qBuilder[self::ENTITY]
        );

        if ($this->qOptions[self::WHERE]) {
            $this->query = sprintf(
                $this->query.self::Q_WHERE,
                $this->qBuilder[self::CONDITION]
            );
        }
    }

    private function buildInsert() {

        $this->query = sprintf(
            self::Q_INSERT,
            $this->qBuilder[self::ENTITY],
            implode(', ', array_keys($this->qBuilder[self::VALUES])),
            ':'.implode(', :', array_keys($this->qBuilder[self::VALUES]))
        );
    }

    private function buildDelete() {

        $this->query = sprintf(
            self::Q_DELETE,
            $this->qBuilder[self::ENTITY]
        );

        if ($this->qOptions[self::WHERE]) {
            $this->query = sprintf(
                $this->query.self::Q_WHERE,
                $this->qBuilder[self::CONDITION]
            );
        }
    }

    private function buildUpdate() {

        $this->query = sprintf(
            self::Q_UPDATE,
            $this->qBuilder[self::ENTITY],
            $this->qBuilder[self::VALUES]
        );

        if ($this->qOptions[self::WHERE]) {
            $this->query = sprintf(
                $this->query.self::Q_WHERE,
                $this->qBuilder[self::CONDITION]
            );
        }

        echo $this->query;
    }

    public function clear() {

        $this->qBuilder = [
            self::TYPE => self::SELECT,
            self::ENTITY => null,
            self::VALUES => null,
            self::CONDITION => null,
        ];

        $this->qOptions = [
            self::WHERE => false
        ];

        $this->clean();
    }
}