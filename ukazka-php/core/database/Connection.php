<?php


class Connection
{
    /**
     * Connects application to database.
     * @return pdo database connection
     */
    public static function connect($config) {
        try {
            $dsn = "mysql:host={$config['servername']};dbname={$config['dbname']};charset={$config['charset']}";
            $pdo = new PDO($dsn, $config['username'], $config['password'], $config['options']);
            return $pdo;
        } catch(PDOException $e) {
            die($e->getMessage());
        }
    }

    /**
     * Connects application to server (init-script purposes).
     */
    public static function connectToServer($config) {
        try {
            $dsn = "mysql:host={$config['servername']}";
            $pdo = new PDO($dsn, $config['username'], $config['password'], $config['options']);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $pdo;
        } catch(PDOException $e) {
            die($e->getMessage());
        }
    }

}