<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

        <link rel="stylesheet" href="/public/css/style.css">

        <title>Document</title>
    </head>
    <body>

        <header>
            <nav class="nav-header-main">
                <a class="header-logo" href="#">
                    <span>Logo</span>
                </a>
                <ul class="nav-wrapper">
                    <li class="<?=Request::uri() == '' ? 'active' : '' ?>"><a href="/">Home</a></li>
                    <li class="<?=Request::uri() == 'portfolio' ? 'active' : '' ?>"><a href="/portfolio">Portfolio</a></li>
                    <li class="<?=Request::uri() == 'about' ? 'active' : '' ?>"><a href="/about">About me</a></li>
                    <li class="<?=Request::uri() == 'contact' ? 'active' : '' ?>"><a href="/contact">Contact</a></li>
                </ul>
                <div class="header-logged-in">
                    <?php if (!isset($_SESSION['user_id'])) :?>
                        <a href="/signup">Sign Up</a>
                    <?php else: ?>
                        <nav class="dropdown">
                            <ul>
                                <li><?=$_SESSION['user_username'] ?>
                                    <ul class="dropdown-menu menu-1">
                                        <li><a href="/admin/profile">Profile</a></li>
                                        <li><a href="/admin/tattoo">Add tattos</a></li>
                                        <li>
                                            <form class="logout" action="/logout" method="POST">
                                                <button type="submit" name="logout">Log Out</button>
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    <?php endif; ?>
                </div>
            </nav>
        </header>
