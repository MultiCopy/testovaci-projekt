<?php
require('views/base/header.php')
?>

    <main>
        <div class="wrapper-main">
            <section class="section-signup">
                <h1>Signup</h1>
                <form class="form-signup" name="signup-form" action="/signup" method="POST" onsubmit="return validateForm()">
                    <input type="text" name="username" placeholder="Username.." value="<?= isset($_GET['username']) ? $_GET['username'] : '' ?>">
                    <p id="uid" class="error"><?= isset($_GET['error']) ? 'Username is taken!' : ''?></p>
                    <input type="text" name="email" placeholder="Email.." value="<?= isset($_GET['email']) ? $_GET['email'] : '' ?>">
                    <p id="umail" class="error"></p>
                    <input type="password" name="password" placeholder="Password..">
                    <p id="upwd" class="error"></p>
                    <input on type="password" name="password_repeat"  placeholder="Repeat Password..">
                    <p id="upwdr" class="error"></p>
                    <button type="submit" name="signup">Sign Up</button>
                </form>
            </section>
        </div>
    </main>

    <script>
        $('input').keypress(function(e){
            var name = e.target.getAttribute('name');
            if (name == 'username') {
                $('#uid').text('');
            } else if (name == 'email') {
                $('#umail').text('');
            } else if (name == 'password') {
                $('#upwd').text('');
            } else if (name == 'password_repeat') {
                $('#upwdr').text('');
            }
        });


        function validateForm() {
            var valid = true;
            var form = document.forms['signup-form'];

            if (form['username'].value == '') {
                $('#uid').text('Username cannot be empty!');
                valid = false;
            } else { $('#uid').text(''); }

            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (form['email'].value == '') {
                $('#umail').text('Email cannot be empty!');
                valid = false;
            } else if (!re.test(String(form['email'].value).toLowerCase())) {
                $('#umail').text('Email is not valid!');
            } else { $('#umail').text(''); }



            if (form['password'].value == '') {
                $('#upwd').text('Password cannot be empty!');
                valid = false;
            } else { $('#upwd').text(''); }
            if (form['password_repeat'].value == '') {
                $('#upwdr').text('Repeat Password cannot be empty!');
                valid = false;
            } else if (form['password'].value !== form['password_repeat'].value && form['password'].value != '') {
                $('#upwdr').text('Passwords does not match!');
                valid = false;
            } else { $('#upwdr').text(''); }

            if (!valid) {
                form['password'].value = '';
                form['password_repeat'].value = '';
            }

            return valid;
        }
    </script>

<?php
require('views/base/footer.php')
?>
