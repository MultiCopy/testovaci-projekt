<?php
require('views/base/header.php')
?>

    <main>
        <div class="wrapper-main">
            <section class="section-login">
                <h1>Log in!</h1>
                <form class="login" name="login-form" action="/login" method="post" onsubmit="return validateLoginForm()">
                    <input type="text" name="username" placeholder="Username..">
                    <input type="password" name="password" placeholder="Password..">
                    <button type="submit" name="login">Log In</button>
                    <a href="/forgotten-password">Forgot Your Password?</a>
                </form>
            </section>
        </div>
    </main>


    <script>
        function validateLoginForm() {
            var form = document.forms['login-form'];
            if (form['username'].value == '' || form['password'].value == '') {
                return false;
            }

            return true;
        }
    </script>
<?php
require('views/base/footer.php')
?>


