<?php
    require('views/base/header.php')
?>

    <main>
        <div class="wrapper-main">
            <form action="add-tattoo" name="upload-form" method="POST" enctype="multipart/form-data" onsubmit="return validateUploadForm()">
                <input type="text" name="title" placeholder="Title..">
                <textarea name="description" placeholder="Description.."></textarea>
                <input type="file" name="image" >
                <button type="submit" name="upload-tattoo">Submit</button>
            </form>

            <div class="tattoos">
                <?php foreach ($tattoos as $tattoo): ?>
                    <h2 class="title"><?=$tattoo->getTitle() ?></h2>
                    <label>Displayed:</label><span><?=$tattoo->getDisplayed() == '1' ? '&#x2714;' : '&#x2717;' ?></span>
                    <img src="/tattoos/<?=$tattoo->getImage() ?>" alt="tattoo">
                    <div><p><?=$tattoo->getDescription() ?></p></div>
                <?php endforeach; ?>
            </div>
        </div>
    </main>

    <script>
        function validateUploadForm() {
            var form = document.forms['upload-form'];
            if (form['image'].value == '') {
                return false;
            }

            return true;
        }
    </script>
<?php
    require('views/base/footer.php')
?>


