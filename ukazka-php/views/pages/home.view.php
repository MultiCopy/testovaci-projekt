<?php
require('views/base/header.php')
?>

    <main>
        <div class="wrapper-main">
            <section class="section-default">
                <?php if (isset($_SESSION['user_id'])) :?>
                    <p class="text">You are logged in!</p>
                <?php else: ?>
                    <p class="text">You are logged out!</p>
                <?php endif; ?>
            </section>
        </div>
    </main>

<?php
require('views/base/footer.php')
?>
