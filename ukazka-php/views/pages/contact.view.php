<?php
    require('views/base/header.php')
?>

    <main>
        <?php
            $messages->display();
        ?>

        <div class="wrapper-main">
            <section class="section-default">
                <p class="text">Contact</p>
                <form name="book" action="/book" method="POST">
                    <input name="contact" type="text" placeholder="Email, phone, facebook...">
                    <textarea name="description" id="" value="napis nieco.."></textarea>
                    <button name="book_submit" type="submit">Contact</button>
                </form>
            </section>
        </div>
    </main>

<?php
    require('views/base/footer.php')
?>
