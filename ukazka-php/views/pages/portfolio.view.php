<?php
require('views/base/header.php')
?>

    <main>
        <div class="wrapper-main">
            <section class="section-default">
                <p class="text">Portfolio</p>
            </section>

            <div class="tattoos">
                <?php foreach ($tattoos as $tattoo): ?>
                    <h2 class="title"><?=$tattoo->getTitle() ?></h2>
                    <img src="/tattoos/<?=$tattoo->getImage() ?>" alt="tattoo">
                    <div><p><?=$tattoo->getDescription() ?></p></div>
                <?php endforeach; ?>
            </div>
        </div>
    </main>

<?php
require('views/base/footer.php')
?>
