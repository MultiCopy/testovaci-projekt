<?php

return [
    'database' => [
        'servername' => 'localhost',
        'username' => 'root',
        'password' => 'admin',
        'dbname' => 'tattoo',
        'charset' => 'utf8mb4',
        'options' => [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]
    ]
];