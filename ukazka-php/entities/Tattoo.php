<?php

class Tattoo
{

    private $id;
    private $title;
    private $image;
    private $description;
    private $displayed;

    public function getId() { return $this->id; }
    public function setId($id) { $this->id = $id; }
    public function getTitle() { return $this->title; }
    public function setTitle($title) { $this->title = $title; }
    public function getImage() { return $this->image; }
    public function setImage($image) { $this->image = $image; }
    public function getDescription() { return $this->description; }
    public function setDescription($description) { $this->description = $description; }
    public function getDisplayed() { return $this->displayed; }
    public function setDisplayed($displayed) { $this->displayed = $displayed; }

}