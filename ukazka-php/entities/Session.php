<?php

class Session {

    private $id;
    private $token;
    private $serial;
    private $user_id;
    private $date;

    public function getId() { return $this->id; }
    public function setId($id) { $this->id = $id; }
    public function getToken() { return $this->token; }
    public function setToken($token) { $this->token = $token; }
    public function getSerial() { return $this->serial; }
    public function setSerial($serial) { $this->serial = $serial; }
    public function getUserId() { return $this->user_id; }
    public function setUserId($user_id) { $this->user_id = $user_id; }
    public function getDate() { return $this->date; }
    public function setDate($date) { $this->date = $date; }

}