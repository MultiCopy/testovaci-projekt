<?php

$router->get('', 'PagesController@home');
$router->get('portfolio', 'PagesController@portfolio');
$router->get('about', 'PagesController@about');
$router->get('contact', 'PagesController@contact');
$router->get('admin/auth', 'PagesController@adminAuthentication');
$router->get('admin/profile', 'PagesController@adminProfile');
$router->get('admin/tattoo', 'PagesController@adminTattoo');

$router->post('login', 'SecurityController@login');
$router->post('logout', 'SecurityController@logout');
$router->post('signup', 'SecurityController@signup');
$router->post('admin/add-tattoo', 'TattooController@addTattoo');
$router->post('book', 'BookingController@book');


$router->get('test', 'PagesController@testingInterface');