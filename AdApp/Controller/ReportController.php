<?php

/**
 * Class ReportController ensure processing reports, e.g. requesting or displaying data.
 */
class ReportController {

    /**
     * Method request data from advert system specified in POST parameter and store
     * them in the database. Requesting data is also dependent on other parameters like
     * dateFrom, itemLimit etc.
     */
    public function requestReport() { }

    /**
     * Method select report data from database and display them into the view ReportOverview.
     */
    public function displayReports() { }

}