<?php

/**
 * Class Router map routes to controller methods and process redirects between URIs.
 */
class Router {

    /**
     * @var array
     */
    private $routes = [
        'GET' => [],
        'POST' => []
    ];

    /**
     * Method create Router class instance depended on file specified. File must contain
     * routs definitions using methods get($uri, $controller), post($uri, $controller) or
     * define($routes).
     * @return object class instance
     */
    public static function load($file) { }

    /**
     * Method map GET request uri on controller method.
     * @param $uri URI
     * @param $controller method of controller specified as "controller_name@controller_method"
     */
    public function get($uri, $controller) { }

    /**
     * Method map POST request uri on controller method.
     * @param $uri URI
     * @param $controller method of controller specified as "controller_name@controller_method"
     */
    public function post($uri, $controller) { }

    /**
     * Method map GET and POST URIs based on array specified.
     * @param $routes array of GET and POST URIs
     */
    public function define($routes) { }

    /**
     * Method process requests and calls competent controller method.
     * @param $uri String
     * @param $requestType GET, POST, PUT, DELETE
     */
    public function direct($uri, $requestType) { }
}