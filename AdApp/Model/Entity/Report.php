<?php

/**
 * Class Report is an entity of a report.
 */
class Report extends Entity{

    private $id;
    private $date;
    private $ad_camping;
    private $ad_group;
    private $impressions;
    private $clicks;
    private $cost;
    private $conversions;
    private $keywords;

    /**
     * Method select keywords associated with this Report entity.
     */
    public function selectKeywords() { }

    public function __get($name) { }
    public function __set($name, $value) { }

}