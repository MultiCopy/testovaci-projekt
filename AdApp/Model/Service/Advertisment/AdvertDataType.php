<?php

/**
 * Class AdvertDataType contains data types of AdvertSystems. It's defined for
 * requesting data from advert system due to utilization of adapter design pattern
 * of AdvertSystem classes.
 */
class AdvertDataType {

    const GROUP_REPORT = 3;

    const KEYWORD = 4;

}