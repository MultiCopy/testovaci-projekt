<?php


/**
 * Class AdvertSystem ensure request data from AdvertSystem, it's
 */
abstract class AdvertSystem {

    /**
     * Method build session for the competent AdvertSystem
     */
    abstract public function login();

    /**
     * Method request report data from Advert System based on parameters specified.
     * Response data are converted and formatted into uniform format using ReportFormatter.
     * @param $reportType type of a report (e.g. AdvertDataType::GROUP_REPORT)
     * @param $params parameters of requesting report (e.g. scope, aggregation, item limit etc.)
     * @return array converted response report into uniform format
     */
    abstract public function requestReport($reportType, $params);

    /**
     * Method request report data from Advert System based on parameters specified.
     * Response data are converted and formatted into uniform format using ReportFormatter.
     * @param $advDataType type of a report (e.g. AdvertDataType::KEYWORD)
     * @param $params parameters of requesting report (e.g. scope, aggregation, item limit etc.)
     * @return array converted response data into uniform format
     */
    abstract public function requestData($advDataType, $params);

}