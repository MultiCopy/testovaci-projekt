<?php


/**
 * Class SklikAds ensure request data from Sklik, it's
 */
class SklikAds extends AdvertSystem {

    /**
     * @var String session
     */
    private $session;
    /**
     * @var String api url
     */
    private $url;

    /**
     * Method build session for the competent AdvertSystem
     */
    public function login() { }

    /**
     * Method request report data from Advert System based on parameters specified.
     * Response data are converted and formatted into uniform format using ReportFormatter.
     * @param $reportType AdvertDataType type of a report (e.g. AdvertDataType::GROUP_REPORT)
     * @param $params array parameters of requesting report (e.g. scope, aggregation, item limit etc.)
     * @return array converted response data into uniform format
     */
    public function requestReport($reportType, $params) { }

    /**
     * Method request report data from Advert System based on parameters specified.
     * Response data are converted and formatted into uniform format using ReportFormatter.
     * @param $advDataType AdvertDataType type of a report (e.g. AdvertDataType::KEYWORD)
     * @param $params array parameters of requesting report (e.g. scope, aggregation, item limit etc.)
     * @return array converted response data into uniform format
     */
    public function requestData($advDataType, $params) { }

    /**
     * Method build header for request data.
     * @param $url String request url
     * @param $request array params of request
     */
    private function buildHeader($url, $request) { }

    /**
     * Method check for errors in response.
     * @params $object stdCLass response data
     * @return int errorCode;
     */
    private function checkErrors($object) { }
}