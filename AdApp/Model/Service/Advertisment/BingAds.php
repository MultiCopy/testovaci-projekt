<?php


/**
 * Class BingAds ensure request data from Bing, it's
 */
class BingAds extends AdvertSystem {

    /**
     * @var array session
     */
    private $session;
    /**
     * @var int format of report
     */
    private $reportFormat = ReportFormat::tsv;

    /**
     * Method build session for the competent AdvertSystem
     */
    public function login() { }

    /**
     * Method request report data from Advert System based on parameters specified.
     * Response data are converted and formatted into uniform format using ReportFormatter.
     * @param $reportType AdvertDataType type of a report (e.g. AdvertDataType::GROUP_REPORT)
     * @param $params array parameters of requesting report (e.g. scope, aggregation, item limit etc.)
     * @return array array converted response report into uniform format
     */
    public function requestReport($reportType, $params) { }

    /**
     * Method request report data from Advert System based on parameters specified.
     * Response data are converted and formatted into uniform format using ReportFormatter.
     * @param $advDataType AdvertDataType type of a report (e.g. AdvertDataType::KEYWORD)
     * @param $params array parameters of requesting report (e.g. scope, aggregation, item limit etc.)
     * @return array array converted response data into uniform format
     */
    public function requestData($advDataType, $params) { }

    /**
     * Method encode report into Bing request based on specified parameters.
     * @param $requestType ReportRequest Bing library ReportRequest
     * @param $aggregations Aggregation aggregations
     * @param $scope Scope scope
     */
    private function encodeReport($requestType, $aggregations, $scope) {}

}