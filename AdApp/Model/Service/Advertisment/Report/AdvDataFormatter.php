<?php

/**
 * Class AdvDataFormatter convert data to array
 */
class AdvDataFormatter {

    const CSV = 1;
    const JSON = 2;
    const TSV = 3;

    /**
     * Method call converting method based on data format (CSV, JSON...)
     * @param $data mixed to convert
     * @param $format int format of data to convert from
     * @return array converted data to array
     */
    public static function format($data, $format) { }

    /**
     * Method convert data to array from CSV
     * @return array converted data to array
     */
    private static function convertCSV($data) { }

    /**
     * Method convert data to array from JSON
     * @return array converted data to array
     */
    private static function convertJSON($data) { }

    /**
     * Method convert data to array from TSV
     * @return array converted data to array
     */
    private static Function convertTSV($data) { }

}