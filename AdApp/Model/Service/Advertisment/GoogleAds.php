<?php


/**
 * Class GoogleAds ensure request data from Google, it's
 */
class GoogleAds extends AdvertSystem {

    /**
     * @var AdWordsSession session
     */
    private $session;
    /**
     * @var int format of downloaded data
     */
    private $downloadFormat =  DownloadFormat::CSV;

    /**
     * Method build session for the competent AdvertSystem
     */
    public function login() { }

    /**
     * Method request report data from Advert System based on parameters specified.
     * Response data are converted and formatted into uniform format using ReportFormatter.
     * @param $reportType AdvertDataType type of a report (e.g. AdvertDataType::GROUP_REPORT)
     * @param $params array parameters of requesting report (e.g. scope, aggregation, item limit etc.)
     * @return array converted response report into uniform format
     */
    public function requestReport($reportType, $params) { }

    /**
     * Method request report data from Advert System based on parameters specified.
     * Response data are converted and formatted into uniform format using ReportFormatter.
     * @param $advDataType AdvertDataType type of a report (e.g. AdvertDataType::KEYWORD)
     * @param $params array parameters of requesting report (e.g. scope, aggregation, item limit etc.)
     * @return array converted response data into uniform format
     */
    public function requestData($advDataType, $params) { }

    /**
     * Method setup selector based on parameters specified in array.
     * @param $params array parameters for setting up selector
     */
    private function setUpSelector($params) { }

    /**
     * Method setup report definition.
     * @param $selector Selector google lib "query" for requesting data
     * @param $params reprot definition options
     */
    private function setUpReportDef($selector, $params) { }

    /**
     * Method setup report options and settings.
     * @param $params array report options and settings
     */
    private function setUpReportOpt($params) { }

    /**
     * Method build header for request data.
     * @param $selector Selector google lib "query" for requesting data
     */
    private function buildHeader($selector) { }

    /**
     * Method check for errors in response.
     * @return int errorCode;
     */
    private function ceckErrors() { }

}