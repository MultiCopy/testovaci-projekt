<?php


/**
 * Class YahooAds ensure request data from Yahoo, it's
 */
class YahooAds extends AdvertSystem {

    /**
     * @var
     */
    private $session;
    /**
     * @var
     */
    private $objectApi;
    /**
     * @var
     */
    private $cubeApi;

    /**
     * Method build session for the competent AdvertSystem
     */
    public function login() { }

    /**
     * Method request report data from Advert System based on parameters specified.
     * Response data are converted and formatted into uniform format using ReportFormatter.
     * @param $reportType AdvertDataType type of a report (e.g. AdvertDataType::GROUP_REPORT)
     * @param $params array parameters of requesting report (e.g. scope, aggregation, item limit etc.)
     * @return array converted response report into uniform format
     */
    public function requestReport($reportType, $params) { }

    /**
     * Method request report data from Advert System based on parameters specified.
     * Response data are converted and formatted into uniform format using ReportFormatter.
     * @param $advDataType AdvertDataType type of a report (e.g. AdvertDataType::KEYWORD)
     * @param $params array parameters of requesting report (e.g. scope, aggregation, item limit etc.)
     * @return array converted response data into uniform format
     */
    public function requestData($advDataType, $params) { }

    /**
     * Method build header for request data.
     * @param $uri String request url
     * @param $params array params of request
     */
    private function buildHeader($uri, $params) { }

}