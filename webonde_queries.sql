SELECT r.id, g.group_name, c.campaign_name, (r.cost * r.clicks) AS total_price 
FROM Report r JOIN AdGroup g JOIN Campaign c 
ON (
	(r.group_id = g.group_id AND r.as_id = g.as_id) 
	AND 
	(g.campaign_id = c.campaign_id AND g.as_id = c.as_id)
) 
ORDER BY total_price DESC


SELECT r.id, g.group_name, c.campaign_name, r.date, r.clicks, r.cost, DATEDIFF(NOW(), r.date) / (r.clicks * r.cost) AS daily_price 
FROM Report r JOIN AdGroup g JOIN Campaign c 
ON (
	(r.group_id = g.group_id AND r.as_id = g.as_id) 
	AND 
	(g.campaign_id = c.campaign_id AND g.as_id = c.as_id)
) 
ORDER BY daily_price DESC

SELECT r.id, g.group_name, c.campaign_name, r.date, r.impressions, r.clicks, (rw.clicks / rw.impressions) AS interest 
FROM Report r JOIN AdGroup g JOIN Campaign c 
ON (
	(r.group_id = g.group_id AND r.as_id = g.as_id) 
	AND 
	(g.campaign_id = c.campaign_id AND g.as_id = c.as_id)
) 
ORDER BY interest DESC

SELECT c.campaign_name, SUM(r.clicks * r.cost) AS total_cost 
FROM Report r JOIN AdGroup g JOIN Campaign c 
ON (
	(r.group_id = g.group_id AND r.as_id = g.as_id) 
	AND 
	(g.campaign_id = c.campaign_id AND g.as_id = c.as_id)
) 
GROUP BY campaign_name 
ORDER BY total_cost


SELECT c.campaign_name, (SUM(r.clicks / r.impressions) / COUNT(c.campaign_name)) as campaign_interest 
FROM Report r JOIN AdGroup g JOIN Campaign c 
ON (
	(r.group_id = g.group_id AND r.as_id = g.as_id) 
	AND 
	(g.campaign_id = c.campaign_id AND g.as_id = c.as_id)
) 
GROUP BY campaign_name 
HAVING campaign_interest > 0.1
ORDER BY campaign_interest



